# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

#Napišite dio koda koji ispisuje redom brojeve od 1 do 100 koristeći while i for petlju.
count=1
while(count<100):
    print count
    count=count+1
    
for count in range (1,100):
    print count

# <codecell>

#Što nije u redu sa slijedećom funkcijom:
#Funkcija vrati rezultat prije nego sto ispise "the answer is"

# <codecell>

#Što ispisuje slijedeci programski kod:
#Programski kod ne ispisuje nista. Ono sto radi je da u varijablu a stavi niz brojeva, kopira to u varijablu b, i promijeni zadnji element niza a u vrijednostt 6

# <codecell>

#Definirajte funkciju pot koja prima 2 argumenta x i y te računa i vraća potenciju xy . Ukoliko se prilikom poziva ne unese y funkcija vraća x2 
def pot(x,y=2):
    return x**y
pot(2,3)
pot(2)

# <codecell>

pot(2,3)

# <codecell>

#Koja je razlika izmedju mutable i immutable tipova podataka, navedite dva tipa za svaku od kategorija.
#Immutable: bool, str, int
#mutable: list, dict
#Kad promijenimo vrijednost immutable tipu podataka, promijeni se ID te varijable, dok kad mutable varijabli to nije slucaj

# <codecell>

#Napišite kod koji definira mapu kvadrati u kojoj su ključevi redni brojevi od -3 do 3, a vrijednosti kvadrati ključeva zatim iz mape dohvatite kvadrat broja -1 i korijen(e) broja 9. Zašto ne možemo imati odgovarajuću mapu korijeni?
kvadrati={-3:9, -2:4, -1:1, 0:0, 1:1, 2:4, 3:9}
kvadrati[-1]

# <codecell>


