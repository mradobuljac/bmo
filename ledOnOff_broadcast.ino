#include <JeeLib.h>

//portnumber na koji je spojen BlinPlug
BlinkPlug led_button(1); 
/*varijabla koja se šalje svim čvorovima u mreži
te ovisno o primljenoj vrijednosti pali se određena
ledica na BlinkPlug
*/
byte x = 0;

/*setup funkcija gdje se obavlja početna inicijalizacija
varijabli pinova i sl.*/
void setup(){
  Serial.begin(57600);
  //prilikom inicijalizacije, pali redom zelenu i crvenu kao znak da 
  //se je sketch dobro upload-ao
  led_button.ledOn(1);
  delay(750);
  led_button.ledOff(1);
  led_button.ledOn(2);
  delay(750);
  led_button.ledOff(2);
  //kako svaki JeeNode konfiguriramo preko Serial monitora,
  //ova funkcija služi da dohvati ID, grupu iz EEPROM memorije
  rf12_configSilent(); 
}

void loop(){
  //switch stanja
  switch (led_button.buttonCheck()) {
    //stanje ako se pritisne gumb zelene ledice
    case BlinkPlug::ON1:  
      //vrijednost koju šaljemo, kako bi kasnije znali koju ledicu paliti
      x = 1;
      //funkcija koja obavlja slanje (broadcast) svim čvorovima u mreži
      rf12_sendNow(0,&x,sizeof(x));
      break;
   //stanje ako se pritisne gumb crvene ledice
   case BlinkPlug::ON2:  
      //vrijednost koju šaljemo, kako bi kasnije znali koju ledicu paliti
      x = 2;
      //funkcija koja obavlja slanje (broadcast) svim čvorovima u mreži
      rf12_sendNow(0, &x, sizeof(x));
      break;
   }
  
  //uzastopno provjeravamo ako smo primili paket
  //ako funkcija vrati true, znači da je paket primljen
  if(rf12_recvDone()){
    //ako je vrijednost koju smo primili jednaka 1, pali zelenu ledicu
    if((*rf12_data) == 1 ){
      led_button.ledOn(1);
      delay(1500);
      led_button.ledOff(1);
    }
    //ako je vrijednost koju smo primili jednaka 2, pali crvenu ledicu
    if( (*rf12_data) == 2 ){
      led_button.ledOn(2);
      delay(1500);
      led_button.ledOff(2);
    }
  }
}
