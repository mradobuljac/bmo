/// ledOnOff_broadcasting
/// algoritam za drugu laboratorijsku vježbu gdje je cilj pokazati studentima slanje
/// i primanje paketa te kontrola svjetljenja ledica
/// Students: Radobuljac, Siljan

#include <JeeLib.h>

BlinkPlug led_button(1); //portnumber na koji je spojen BlinPlug
byte x = 0;

void setup(){
  Serial.begin(57600);
  rf12_configSilent(); 
}

void loop(){
  
  switch (led_button.buttonCheck()) {
    case BlinkPlug::ON1:  
      x = 1;
      rf12_sendNow(0,&x,sizeof(x));
      break;
   case BlinkPlug::ON2:  
      x = 2;
      rf12_sendNow(0, &x, sizeof(x));
      break;
   }
  
  if(rf12_recvDone()){
    if((*rf12_data) == 1 ){
      led_button.ledOn(1);
      delay(1500);
      led_button.ledOff(1);
    }else{
      led_button.ledOn(2);
      delay(1500);
      led_button.ledOff(2);
    }
  }
}