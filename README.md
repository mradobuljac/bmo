09.03.2014.
===========

Napravio repo


12.03.2014.
===========

Upload djela zadataka
Nadam se da je format ispravan

28.04.2014.
===========

-Od proslog commita do sad: proucavanje zadatka, proucavanje opreme, nabavka opreme, konzultacija s profesorom.
-Problemi s USB BUB-om, tocnije nedostatkom USB BUB-a. Samo je jedan BUB na dva projekta

02.05.2014.
===========

-Rijesen prvi dio zadatka (uspjesno spajanje i pokretanje JeeNodesa i slanje podataka na ostalu periferiju)
-Rijesen prvi dio druge vjezbe (JeeNode s LDR-om)
-Isplaniran drugi dio druge vjezba (LED dioda s JeeNodesima)
-Sljedece: Izrada dokumentacije za vjezbu


25.05.2014.
===========

-Izradjena dokumentacija za prvi dio vjezbe (Upoznavanje s JeeNodes i slanje osnovne poruke)
-Izradjena dokumentacija za prvu laboratorijsku vjezbu (LDR)
-Odradjene dvije konzultacije s profesorom
-Izrada druge laboratorijske vjezbe u tijeku (BlinkPlug)

03.06.2014.
===========

-Dovrsena druga laboratorijska vjezba, prijenos signala s jednog cvora na drugi
-Tocnije, pritiskom na jedno od dugmeta BlinkPlug-a, ta informacija se prenese do drugog cvora i potom odgovarajuca LED dioda zasvijetli na udaljenom cvoru
-Dogovorene konzultacije u cetvrtak
-Ako sav prakticni dio bude kako treba, ostaje dovrsiti dokumentaciju i upute
-Uploadan i src kod svih vjezbi


10.06.2014.
===========

-Konacni (vjerojatno) commit
-Commitani dokumenti za prvu vjezbu
-Commitani svi source kodovi
